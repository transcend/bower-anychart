/**
 * @ngdoc overview
 * @name transcend.anychart
 * @description
 # AnyChart Module
 The "AnyChart" module provides a set of reusable components to quickly and easily create charts/graphs using the
 [AnyChart](http://www.anychart.com/home/) library. The main benefit of the components available in this module is the
 ability to bind charts to data and allow them to automatically update when the data changes - using the standard
 [AngularJS data binding technology](http://docs.angularjs.org/guide/databinding).

 ## Installation
 The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
 commands to install this package:

 ```
 bower install https://bitbucket.org/transcend/bower-anychart.git
 ```

 Other options:

 *   * Download the code at [http://code.tsstools.com/bower-anychart/get/master.zip](http://code.tsstools.com/bower-anychart/get/master.zip)
 *   * View the repository at [http://code.tsstools.com/bower-anychart](http://code.tsstools.com/bower-anychart)
 *   * If you have permissions, view the source code at [http://code.tsstools.com/web-common](http://code.tsstools.com/web-common/src/ee541a3bce850ae77f6d7948a00cc352b4f67b21/modules/anychart/?at=develop)

 When you have pulled down the code and included the files (and dependency files), simply add the module as a
 dependency to your application:

 ```
 angular.module('myModule', ['transcend.anychart']);
 ```

 ## To Do
 - Find and create directives based on another charting library - we need to deprecate our use of this library.
 - Re-evaluate the approach of storing the AnyChart library code in the repo.

 ## Project Goals
 - Find a better library to build upon - AnyChart isn't cutting it.
 */
/**
 * @ngdoc service
 * @name transcend.anychart.service:$anyChart
 *
 * @description
 * The '$anyChart' service is a factory to create AnyChart instances. The factory is just a wrapper to the global
 * window.AnyChart object. The object itself is the actual AnyChart library instance. This service simply pulls it
 * off the window to allow it to be created via an angular factory call so that it can be injected in a cleaner manner.
 *
 * Typically this service will not be used in application code but rather in the the {@link transcend.anychart:anyChart AnyChart} directive.
 *
 * @requires $window
 *
 * @example
 <pre>
 app.controller('TestCtrl', function ($anyChart) {
   // Create a new AnyChart instance from the factory.
   chart = $anyChart();
   chart.addEventListener('render', postRender);
   chart.width = '400px';
   chart.height = '400px';
 });
 </pre>
 */
/**
 * @ngdoc directive
 * @name transcend.anychart.directive:anyChart
 *
 * @description
 * The 'AnyChart' directive provides the ability to create "AnyChart" graphs and charts and bind to dynamic data.
 *
 * Note: You need the AnyChart license file and chart configurations to use AnyChart.
 *
 * @restrict EA
 * @element ANY
 * @scope
 *
 * @requires $timeout
 * @requires $http
 * @requires transcend.core:tsConfig
 * @requires transcend.anychart:$anyChart
 *
 * @param {bool=} pie Flag to determine if the chart should be of type 'pie'.
 * @param {bool=} gauge Flag to determine if the chart should be of type 'gauge'.
 * @param {bool=} column Flag to determine if the chart should be of type 'column'.
 * @param {string=} config Path to the configuration to use to build the chart. This configuration xml holds information
 * like series colors, labels, etc.
 * @param {string} data Data object to use to populate the chart/grid.
 * @example
 <example module="app">
 <file name="index.html">
 <div ng-controller="Ctrl">
 <button class="btn btn-primary" ng-click="pieValue=browserData">Show Browser Data</button>
 <button class="btn btn-primary" ng-click="pieValue=osData">Show Operating System Data</button>
 <any-chart pie data="pieValue" title="3D Pie Chart" height="350px"></any-chart>

 <hr/>

 <label>
 Gauge Value:
 <input type="number" ng-model="gaugeValue" step="1" min="0" max="100" />
 </label>
 <any-chart gauge data="gaugeValue" title="Percentage Gauge Chart" height="350px"></any-chart>

 <hr/>

 <button class="btn btn-primary" ng-click="columnValue=browserData">Show Browser Data</button>
 <button class="btn btn-primary" ng-click="columnValue=osData">Show Operating System Data</button>
 <any-chart column data="columnValue" title="3D Column Chart" height="350px"></any-chart>
 </file>
 <file name="script.js">
 angular.module('app', ['transcend.anychart'])

 .value('tsConfig', {
   // Override the default XML locations for this "example" code.
   anyChart: {
     pie: {
       config: './mocks/anychart/config/pie-chart-3d.xml'
     },
     gauge: {
       config: './mocks/anychart/config/percentage-gauge.xml'
     },
     column: {
       config: './mocks/anychart/config/column-chart-3d.xml'
     }
   }
 })

 .controller('Ctrl', function($scope) {
    $scope.browserData = [{key: 'Chrome', value: 65}, {key: 'Firefox', value: 30}, {key: 'IE', value: 5}];
    $scope.osData = [{key: 'Windows', value: 45}, {key: 'Linux', value: 10}, {key: 'Mac IOS', value: 45}];
    $scope.pieValue = $scope.browserData;
    $scope.columnValue = $scope.osData;
    $scope.gaugeValue = 77;
  });
 </file>
 </example>
 */
