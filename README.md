# AnyChart Module
The "AnyChart" module provides a set of reusable components to quickly and easily create charts/graphs using the
[AnyChart](http://www.anychart.com/home/) library. The main benefit of the components available in this module is the
ability to bind charts to data and allow them to automatically update when the data changes - using the standard
[AngularJS data binding technology](http://docs.angularjs.org/guide/databinding).

## Installation
The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
commands to install this package:

```
bower install https://bitbucket.org/transcend/bower-anychart.git
```

Other options:

* Download the code at [http://code.tsstools.com/bower-anychart/get/master.zip](http://code.tsstools.com/bower-anychart/get/master.zip)
* View the repository at [http://code.tsstools.com/bower-anychart](http://code.tsstools.com/bower-anychart)
* If you have permissions, view the source code at [http://code.tsstools.com/web-common](http://code.tsstools.com/web-common/src/ee541a3bce850ae77f6d7948a00cc352b4f67b21/modules/anychart/?at=develop)

When you have pulled down the code and included the files (and dependency files), simply add the module as a
dependency to your application:

```
angular.module('myModule', ['transcend.anychart']);
```

## To Do
- Find and create directives based on another charting library - we need to deprecate our use of this library.
- Re-evaluate the approach of storing the AnyChart library code in the repo.

## Project Goals
- Find a better library to build upon - AnyChart isn't cutting it.